import React from "react";
import { ListGroup } from "react-bootstrap";

const UserTranslations = (props) => {
  return (
    <div id="userTranslations" className="row justify-content-center">
      {props.data &&
        <ListGroup>
          {props.data.map((item, index) => {
            return <ListGroup.Item key={index} >{item}</ListGroup.Item>
          })}
        </ListGroup>}
    </div >
  );
};

export default UserTranslations;
