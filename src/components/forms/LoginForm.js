import React from "react";
import { Button, Form } from "react-bootstrap";

const LoginForm = (props) => {

  // Input text is changed
  const onInputChanged = (e) => {
    props.inputChanged(e);
  };

  // Login button clicked
  const onLoginClicked = () => {
    props.loginClicked();
  };

  return (
    <div>
      <div className="row justify-content-center">
        <Form.Group>
          <Form.Control
            type="text"
            placeholder="What is your name?"
            onChange={onInputChanged}
            maxLength="20" />
        </Form.Group>
      </div>
      <div>
        <Button onClick={onLoginClicked}>Log in</Button>
      </div>
    </div>
  );
};

export default LoginForm;
