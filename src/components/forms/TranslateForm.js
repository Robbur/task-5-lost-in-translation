import React from "react";
import { Form, Button } from "react-bootstrap";

const TranslateForm = (props) => {

  const onInputChanged = (e) => {
    props.inputChanged(e);
  };

  const onTranslateClicked = () => {
    props.translateClicked()
  }

  return (
    <div>
      <div>
        <Form className="row justify-content-center">
          <Form.Group>
            <Form.Control
              type="text"
              placeholder="write some text to translate! maximum 20 characters"
              maxLength="20"
              onChange={onInputChanged}
            />
          </Form.Group>
        </Form>
      </div>
      <div>
        <Button type="submit" variant="primary" onClick={onTranslateClicked}>
          Submit
        </Button>
      </div>
    </div>
  );
};

export default TranslateForm;
