import React, { useState } from "react";
import { Redirect, useHistory } from "react-router-dom";
import { Navbar } from "react-bootstrap";
import logo from '../../assets/NavbarIcon.png';

// LocalStorage Import
import { getStorage, setStorage } from "../../utils/storage";

// Component imports
import LoginForm from "../forms/LoginForm";
import AlertBox from "../alerts/AlertBox";

const LoginPage = () => {
  const [name, setName] = useState("");
  const [show, setShow] = useState(false);
  const isLoggedIn = getStorage('username');
  const alertMessage = "Please provide a name";
  const alertStyle = "warning";
  const history = useHistory();

  const handleInputChanged = (e) => {
    if (show) {
      setShow(false);
    }
    setName(e.target.value.trim());
  };

  const handleLoginClicked = () => {
    if (!name) {
      setShow(true);
    } else {
      setStorage('username', { name: name, translations: [] });
      setShow(false);
      history.replace("/translate");
    }
  };

  return (
    <div className="LoginPage">
      {isLoggedIn && <Redirect to="/translate" />}
      <div>
        <Navbar>
          <Navbar.Brand>Lost in translation</Navbar.Brand>
          <img src={logo} style={{ width: '100px' }} alt="logo" />
        </Navbar>
      </div>
      <div id="loginDiv" className="container">
        <h1>Welcome to Lost in translation!</h1>
        <h2>Log in to get started</h2>
        <LoginForm
          inputChanged={handleInputChanged}
          loginClicked={handleLoginClicked}
        />
        {show && <AlertBox message={alertMessage} alert={alertStyle} />}
      </div>
    </div>
  );
};

export default LoginPage;
