import React from "react";
import { Navbar } from "react-bootstrap";
import { getStorage } from "../../utils/storage";
import logo from '../../assets/NavbarIcon.png';

const NavigationBar = () => {
  const currentUser = getStorage('username');
  return (
    <Navbar>
      <Navbar.Brand href="/translate">Lost in translation</Navbar.Brand>
      <img src={logo} style={{ width: '100px' }} alt="logo" />
      <Navbar.Toggle />
      <Navbar.Collapse className="justify-content-end">
        <Navbar.Text>
          Logged in as: <a href="/profile">{currentUser.name}</a>
        </Navbar.Text>
      </Navbar.Collapse>
    </Navbar>
  );
};

export default NavigationBar;
