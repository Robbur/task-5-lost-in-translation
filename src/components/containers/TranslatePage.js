import React, { useState, useEffect } from 'react';
import { Redirect } from 'react-router-dom';

// LocalStorage Import
import { getStorage, setStorage } from '../../utils/storage';

// Component imports
import NavigationBar from './NavigationBar';
import TranslateForm from '../forms/TranslateForm';
import AlertBox from "../alerts/AlertBox";

const TranslatePage = () => {
  const currentUser = getStorage('username');
  const [showAlert, setShowAlert] = useState(false);
  const [currentInputText, setCurrentInputText] = useState("");
  const [images, setImages] = useState([]);
  const [imageArray, setImageArray] = useState([]);
  const [alertMessage, setAlertMessage] = useState("Only letters will be translated!");
  const [displayText, setDisplayText] = useState("");
  const alertStyle = "warning";

  //regex for letters and space
  let regex = /^[A-Za-z\s]+$/;
  //regex for only letters
  let regex2 = /^[A-Za-z]+$/

  useEffect(() => {
    // images array set to a.png = [0], b.png = [1] etc...
    setImages(importAllImages(require.context('../../assets/signLanguage/', false, /\.(png)$/)));
  }, [])

  // Creates an array of images based on input text
  const createPrintArray = () => {
    let tempArray = [];
    let tempText = "";
    for (let i = 0; i < currentInputText.trim().length; i++) {
      if (regex2.test(currentInputText.charAt(i))) {
        tempArray.push(images[currentInputText.charCodeAt(i) - 97]);
        tempText += currentInputText.charAt(i);
      }
    }
    setDisplayText(tempText);
    setImageArray(tempArray.slice());
  }

  // function to import all images from folder
  const importAllImages = (i) => {
    return i.keys().map(i);
  }

  const handleInputChanged = (inputText) => {
    setCurrentInputText(inputText.target.value.toLowerCase().trim());
  };

  const handleTranslateClicked = () => {
    let isValidText = regex.test(currentInputText);
    //Check is there are numbers or special characters
    if (!isValidText && currentInputText.length > 0) {
      setAlertMessage("Please remove numbers or special characters");
      setShowAlert(true);
      //checks if the input is already saved
    } else if (currentUser.translations.includes(currentInputText)) {
      createPrintArray();
      setShowAlert(false);
      //updates local storage and translates input to images
    } else if (currentInputText.length <= 0) {
      setAlertMessage("please enter a text to translate");
      setShowAlert(true);
    } else {
      updateStorageList(currentInputText);
      createPrintArray();
      setShowAlert(false);
    }
  };

  const updateStorageList = (newTranslation) => {
    const tempUser = {
      ...currentUser,
      translations: functionToPush(currentUser.translations, newTranslation)
    }
    setStorage('username', tempUser);
  };

  //Append new translation to localStorage
  const functionToPush = (currentList, newTranslation) => {
    if (currentList.length >= 10) {
      currentList.shift();
    }
    currentList.push(newTranslation);
    return currentList;
  };

  return (
    <div className="TranslatePage">
      {!currentUser && <Redirect to="/login" />}
      <div>
        <NavigationBar />
      </div>
      <div className="container">
        <h2 id="translateTitle">Translate some text to sign language</h2>
        <TranslateForm
          inputChanged={handleInputChanged}
          translateClicked={handleTranslateClicked}
        />
      </div>
      {showAlert && <AlertBox message={alertMessage} alert={alertStyle} />}
      <div className="container" id="translateOutput">
        {!showAlert &&
          <label>
            {imageArray.map((c, index) => {
              return (
                <div key={index} id="letterDiv">
                  <p>{displayText.charAt(index)}</p>
                  <img src={c} alt="figure" style={{ width: '110px' }} />
                </div>
              );
            })}
          </label>
        }
      </div>
    </div >
  );
};

export default TranslatePage;
