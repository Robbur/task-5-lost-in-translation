import React from "react";
import { Redirect } from "react-router-dom";
import { Button } from "react-bootstrap";

// LocalStorage Import
import { removeStorage, getStorage } from "../../utils/storage";
// Component imports
import NavigationBar from "./NavigationBar";
import UserTranslations from "../outputs/UserTranslations";

const ProfilePage = () => {
  const currentUser = getStorage('username');

  // redirects to login page and Deletes user and translations from localStorage
  const handleLogoutClicked = () => {
    if (window.confirm("Are you sure?")) {
      removeStorage('username');
    }
  };
  return (
    <div>
      {!currentUser && <Redirect to="/login" />}
      <div>
        <NavigationBar />
      </div>
      <div className="container">
        <h2 id="profileTitle">Your translations</h2>
        <UserTranslations data={currentUser.translations} />
      </div>
      <Button variant="primary" href="/" onClick={handleLogoutClicked}>
        Log out
      </Button>
    </div>
  );
};

export default ProfilePage;
