import React from "react";
import { Link } from "react-router-dom";
import AlertBox from "../alerts/AlertBox";

const NotFoundPage = () => {
  const alertMessage = "You are not logged in / the page does not exist";
  const alertStyle = "warning";
  return (
    <div>
      <div>
        <AlertBox message={alertMessage} alert={alertStyle} />
      </div>
      <div>
        <Link to="/login" style={{ color: 'black' }}>Go back to login page</Link>
      </div>
    </div>
  );
};

export default NotFoundPage;
