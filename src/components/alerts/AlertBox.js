import React from "react";
import { Alert } from "react-bootstrap";

const AlertBox = (props) => {
  return (
    <Alert variant={props.alert}>
      <Alert.Heading>Error!</Alert.Heading>
      <p>{props.message}</p>
    </Alert>
  );
};

export default AlertBox;
