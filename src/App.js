import React from "react";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect,
} from "react-router-dom";

import "./App.css";

import LoginPage from "./components/containers/LoginPage";
import ProfilePage from "./components/containers/ProfilePage";
import TranslatePage from "./components/containers/TranslatePage";
import NotFoundPage from "./components/containers/NotFoundPage";

//pass name + array from localstorage to components, to check if logged in
function App() {
  return (
    <Router>
      <div className="App">
        <Switch>
          <Route exact path="/">
            <Redirect to="/login" />
          </Route>
          <Route path="/login" component={LoginPage} />
          <Route path="/profile" component={ProfilePage} />
          <Route path="/translate" component={TranslatePage} />
          <Route path="*" component={NotFoundPage} />
        </Switch>
      </div>
    </Router>
  );
}

export default App;
