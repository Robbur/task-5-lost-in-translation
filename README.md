# JavaScript - lost in translation.

This project was built with the React framework

This website let's you translate text to sign language, letter for letter.

You start by "logging in" with your name or username, which will take you to the translation page.

From here you can navigate to your profile page by clicking on your name/username in the top right corner.
Here you will see your 10 latest translations, and you can also log out from here.

To go back to the translation page, click on the "lost in translation" title in the top left.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

You can start by downloading the project in a .zip file, or cloning the project using the terminal.

### Running the project

Open a terminal, navigate to the project folder and run the following commands

## Project setup
```
npm install
```

### Compiles and run
```
npm start
```

## Maintainers 

[Robin Burø (@Robbur)](https://gitlab.com/Robbur)

## License

---
Copyright 2020, Robin Burø ([@Robbur](https://gitlab.com/Robbur))
